package com.api.rickandmorty.service;

import com.api.rickandmorty.dto.FindCharacterResponseDTO;

public interface FindCharacterSerivice {
	public FindCharacterResponseDTO findCharacter(Integer idCharacter);

}
