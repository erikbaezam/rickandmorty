package com.api.rickandmorty.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindCharacterResponseDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9097593076207172409L;
	
	private Integer id;
	private String name;
	private String status;
	private String species;
	private String type;
	
	@JsonProperty("episode_count")
	private Integer episodeCount;
	
	private SingleOriginResponse origin;


}
