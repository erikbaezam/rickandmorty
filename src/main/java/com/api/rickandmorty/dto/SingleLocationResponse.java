package com.api.rickandmorty.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SingleLocationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4461624279885801600L;
	private Integer Id;
	private String name;
	private String type;
	private String dimension;
	private List<String> residents;
	private String url;
	private Timestamp created;

}
