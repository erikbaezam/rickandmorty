package com.api.rickandmorty.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.api.rickandmorty.dto.FindCharacterResponseDTO;
import com.api.rickandmorty.dto.SingleCharacterResponse;
import com.api.rickandmorty.dto.SingleLocationResponse;
import com.api.rickandmorty.dto.SingleOriginResponse;

@Service
public class FindCharacterServiceImpl implements FindCharacterSerivice {

	RestTemplate restTemplate = new RestTemplate();

	public static final String URI_GET_SINGLE_CHARACTER = "https://rickandmortyapi.com/api/character/";
	public static final String URI_GET_SINGLE_LOCATION = "https://rickandmortyapi.com/api/location/";

	@Override
	public FindCharacterResponseDTO findCharacter(Integer idCharacter) {

		ResponseEntity<SingleCharacterResponse> characterResponseEntity = restTemplate.exchange(
				URI_GET_SINGLE_CHARACTER + idCharacter, HttpMethod.GET, null,
				new ParameterizedTypeReference<SingleCharacterResponse>() {
				});
		SingleCharacterResponse singleCharacterResponse = characterResponseEntity.getBody();
		
		ResponseEntity<SingleLocationResponse> locationResponseEntity = restTemplate.exchange(
				URI_GET_SINGLE_LOCATION + singleCharacterResponse.getLocation().getUrl()
						.split("https://rickandmortyapi.com/api/location/")[1],
				HttpMethod.GET, null, new ParameterizedTypeReference<SingleLocationResponse>() {
				});
		
		SingleLocationResponse singleLocationResponse = locationResponseEntity.getBody();

		return FindCharacterResponseDTO.builder().id(singleCharacterResponse.getId())
				.name(singleCharacterResponse.getName()).status(singleCharacterResponse.getStatus())
				.species(singleCharacterResponse.getSpecies()).type(singleCharacterResponse.getType())
				.episodeCount(singleCharacterResponse.getEpisode().size())
				.origin(SingleOriginResponse.builder().name(singleLocationResponse.getName())
						.url(singleLocationResponse.getUrl()).dimension(singleLocationResponse.getDimension())
						.residents(singleLocationResponse.getResidents()).build())
				.build();

	}

}
