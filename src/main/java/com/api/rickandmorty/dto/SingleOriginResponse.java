package com.api.rickandmorty.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SingleOriginResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -959848556075065294L;
	private String name;
	private String url;
	private String dimension;
	private List<String> residents;

}
