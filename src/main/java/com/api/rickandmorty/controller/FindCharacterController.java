package com.api.rickandmorty.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.api.rickandmorty.dto.FindCharacterResponseDTO;
import com.api.rickandmorty.service.FindCharacterServiceImpl;

@RestController
public class FindCharacterController {

	/**
	 * Inyeccion del service de estado de requerimiento.
	 */
	@Autowired
	private FindCharacterServiceImpl findCharacterService;

	@GetMapping(value = "/find/{characterId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FindCharacterResponseDTO findCharacter(@PathVariable Integer characterId) {
		return findCharacterService.findCharacter(characterId);
	}
}
