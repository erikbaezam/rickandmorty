package com.api.rickandmorty.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SingleCharacterResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -976097693154635798L;
	private Integer id;
	private String name;
	private String status;
	private String species;
	private String type;
	private String gender;
	private SingleOriginResponse origin;
	private SingleLocationResponse location;
	private String image;
	private List<String> episode;
	private Timestamp created;

}
